package yigl.plugga.classdesign;

public class SubClass extends SuperClass {
    int num = 10;

    public SubClass(int num) {
        this.num = num;
    }

    // display method of sub class
    public void display() {
        System.out.println("num = " + num + " -- From subclass");
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void my_method() {
        // Invoking the display() method of sub class
        this.display();

        // Invoking the display() method of superclass
        super.display();

        // printing the value of variable num of subclass
        System.out.println("value of the variable named num in sub class:" + this.num);

        // printing the value of variable num of superclass
        System.out.println("value of the variable named num in super class:" + super.num);
    }

}
