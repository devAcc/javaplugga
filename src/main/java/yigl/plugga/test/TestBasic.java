package yigl.plugga.test;

import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.*;



public class TestBasic extends TestBase {

    /*
     * The difference between program arguments and VM arguments:
     * 1. Program arguments are arguments that are passed to your application, which are accessible via the "args" String array parameter
     *    of your main method.
     * 2. VM arguments are arguments such as System properties that are passed to the Java interpreter(ie,'java'), which is called before Java class
     *    VM arguments are defined with format -DsysProp1="FirstVMargument"
     */
    @Test
    public void processVMArguments() {
        System.out.println("System Properties from VM Arguments");
        String sysProp1 = "sysProp1";
        System.out.println("\tName:" + sysProp1 + ", Value:" + System.getProperty(sysProp1));
        String sysProp2 = "sysProp2";
        System.out.println("\tName:" + sysProp2 + ", Value:" + System.getProperty(sysProp2));

    }


    /**
     * Operate assert
     */
    @Test
    public void testAssert() {
        String str1 = "asdfkk,asd no such";
        String str2 = "asdfkk,asd no such command";
        String str3 = null;

       // assertFalse(str3==null,"str3 empty");

        System.out.println("step1");
        assertFalse(str1==null , "1 failed!");
        System.out.println("step2");
        assertFalse(str1.contains("no such command"), "Assert2 failed!");
        System.out.println("step3");
        assertFalse(str1==null || str1.contains("no such command"), "Assert failed!");
        System.out.println("step4");
        assertTrue(str2.contains("no such command") || str2==null, "step4 failed");
        System.out.println("step5");
        assertFalse(str3==null || str3.contains("no such command"));



    }


    /**
     * Operate LocalDate and LocalTime class
     */
    @Test
    public void operateDateTime() {
        System.out.println(LocalDate.now());
        System.out.println(LocalTime.now());
        System.out.println(LocalDateTime.now());
        System.out.println(ZonedDateTime.now());
    }


    /**
     * Sort array
     *
     */
    @Test
    public void operateArray() {

        int[][] twoD = new int[][]{{1,2},{3,4},{5,6}};
        System.out.println();
        for (int[] inner : twoD) {
            for (int num : inner)
                System.out.print(num + " ");
            System.out.println();
        }

        System.out.println();
        int[][] differentSize = {{1, 4}, {3}, {9,8,7}};
        for (int i = 0; i < differentSize.length; i++) {
            for (int j = 0; j < differentSize[i].length; j++)
                System.out.print(differentSize[i][j] + " ");
            System.out.println();
        }
    }

}