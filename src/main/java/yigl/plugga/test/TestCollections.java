package yigl.plugga.test;

import org.testng.annotations.Test;

import java.util.*;

public class TestCollections extends TestBase{

    /**
     * traverse the whole TreeMap
     */
    @Test
    public void traverseTreeMap() {
        TreeMap<String, Integer> nameAge = new TreeMap<String, Integer>();
        nameAge.put("Lars", 42);
        nameAge.put("sara", 16);
        nameAge.put("Tomas", 36);
        nameAge.put("Martin", 27);
        nameAge.put("Bo", 27);
        nameAge.put("Hanna", 27);
        nameAge.put("Jan", 27);
        nameAge.put("Pelle", 27);

        Set set = nameAge.entrySet();
        Iterator m = set.iterator();
        int sumAge = 0;
        while(m.hasNext()) {
            Map.Entry me = (Map.Entry)m.next();
            sumAge = sumAge + (Integer)me.getValue();
        }

        System.out.println("\nThe name-value is:");
        System.out.print(nameAge);
        System.out.println("\nThe average age is:" + sumAge/nameAge.size());
        System.out.print("Those people are old thant 25 are: ");
        m = set.iterator();
        while(m.hasNext()) {
            Map.Entry me = (Map.Entry)m.next();
            if ((Integer)me.getValue() > 25)
                System.out.print(me.getKey() + ", ");
        }
        System.out.println();
    }

    @Test
    public void sortingCollections() {

        //sorting Array and ArrayList
        int[] a = new int[]{1,8,3,4,5,6,7,9};
        Arrays.sort(a);
        List<Integer> b = new ArrayList<>();
        b.add(23);
        b.add(10);
        b.add(5);
        Collections.sort(b);
    }
}
