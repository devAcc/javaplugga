package yigl.plugga.test;

import org.testng.annotations.Test;
import yigl.plugga.classdesign.*;


import static org.testng.Assert.*;

public class TestClass extends TestBase {


    /**
     * test basic class feature
     */
    @Test
    public void basicClassFeature() {

        // test all class assignment are reference
        SubClass sub1 = new SubClass(20);
        SubClass sub2 = sub1;
        sub2.setNum(30);
        sub1.display();
        System.out.println("Change sub2 will cause sub1 changed");
    }

    /**
     * test Class
     */
    @Test
    public void superClass() {
        // test Super class
        System.out.println("");
        String router$view = "router$view";
        SubClass obj = new SubClass(10);
        obj.my_method();
        System.out.println("variable router$view = " + router$view);

    }

    /**
     * test anonymous Class
     */
    @Test
    public void anonymousClass() {
        SuperClass myApp = new SuperClass();
        myApp.sayHello();
    }

    /**
     * test non-static inner class
     * For non-static inner class, we do need a reference to an object of the enclosing outer class to 
     * instantiate it; 
     */
    @Test
    public void nonStaticInnerClass() {
        SuperClass myApp = new SuperClass();
        SuperClass.NonStaticGreeting greet1 = new SuperClass().new NonStaticGreeting();
        SuperClass.NonStaticGreeting greet2 = myApp.new NonStaticGreeting();
        greet1.greetSomeone("inner non-static class1");
        greet2.greetSomeone("inner non-static class2");
    }

    /**
     * test static inner class
     * 1.Java doesn't allow you to create top-level static classes; only nested (inner) static classes.
     * 2. We don't need an instance of the outer class to create an object of a static inner class. 
     * 3. Static inner classes can access static data members of the enclosing class(static field&method)
     */
    @Test
    public void staticInnerClass() {
        // Compare the difference for the way how instantiate non-static class above.
        SuperClass.StaticGreeting greet = new SuperClass.StaticGreeting();
        greet.greet();


    }

}